import StepOperationMixin from '@paiuolo/pai-vue-mixins/mixins/StepOperationMixin'

export default {
  mixins: [
    StepOperationMixin
  ],
  mounted: function () {
    this.$refs.swiper.$on('slideChanged', (i) => {
      let step = this.getStepKeyByIndex(i)
      this.slideChanged(step)
    })
    this.$on('stepActivated', (step) => {
      let i = this.getStepIndexByKey(step)
      this.swipeTo(i)
    })
  },
  methods: {
    slideChanged () { // (step)
      // console.log('StepOperationSwiperJsMixin slide changed', step)
    },
    swipeTo (i) {
      let allowSlidePrev = this.$refs.swiper.swiper.allowSlidePrev
      let allowSlideNext = this.$refs.swiper.swiper.allowSlideNext
      this.$refs.swiper.swiper.allowSlidePrev = true
      this.$refs.swiper.swiper.allowSlideNext = true

      this.$refs.swiper.swiper.slideTo(i)

      this.$refs.swiper.swiper.allowSlideNext = allowSlideNext
      this.$refs.swiper.swiper.allowSlidePrev = allowSlidePrev
    }
  }
}
